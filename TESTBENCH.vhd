LIBRARY	IEEE;
USE	IEEE.STD_LOGIC_1164.ALL;
USE	IEEE.NUMERIC_STD.ALL;

ENTITY TB_PROC	IS
END TB_PROC;

ARCHITECTURE BEHAVIORAL	OF TB_PROC	IS

	COMPONENT MANAGEMENT_PROCESSOR	IS
		PORT(
			CLOCK		: IN	STD_LOGIC;
			READY		: IN	STD_LOGIC;
			DATA_IN		: IN	UNSIGNED(07 DOWNTO 0);
			DATA_OUT	: OUT	UNSIGNED(07 DOWNTO 0)
		);
	END COMPONENT;

	-- COMMANDS
	-- MEMORY DIRECT
	SIGNAL CAND		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "0000";	-- AND MEMORY WORD TO AC
	SIGNAL CADD		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "0001";	-- ADD MEMORY WORD TO AC
	SIGNAL CLDA		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "0010";	-- LOAD MEMORY WORD TO AC
	SIGNAL CSTA		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "0011";	-- STORE CONTENT OF AC IN MEMORY
	SIGNAL CBUN		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "0100";	-- BRANCH UNCONDITIONALLY
	SIGNAL CBSA		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "0101";	-- BRANCH AND SAVE RETURN ADDRESS
	SIGNAL CISZ		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "0110";	-- INCREMENT AND SKIP IF ZERO
	-- MEMORY INDIRECT
	SIGNAL CIAND		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "1000";	-- AND MEMORY WORD TO AC
	SIGNAL CIADD		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "1001";	-- ADD MEMORY WORD TO AC
	SIGNAL CILDA		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "1010";	-- LOAD MEMORY WORD TO AC
	SIGNAL CISTA		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "1011";	-- STORE CONTENT OF AC IN MEMORY
	SIGNAL CIBUN		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "1100";	-- BRANCH UNCONDITIONALLY
	SIGNAL CIBSA		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "1101";	-- BRANCH AND SAVE RETURN ADDRESS
	SIGNAL CIISZ		: STD_LOGIC_VECTOR(3 DOWNTO 0)	:= "1110";	-- INCREMENT AND SKIP IF ZERO
	-- REGISTERS
	SIGNAL CCLA		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7800";	-- CLEAR AC
	SIGNAL CCLE		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7400";	-- CLEAR E
	SIGNAL CCMA		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7200";	-- COMPLEMENT AC
	SIGNAL CCME		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7100";	-- COMPLEMENT E
	SIGNAL CCIR		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7080";	-- CIRCULATE RIGHT AC AND E
	SIGNAL CCIL		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7040";	-- CIRCULATE LEFT AC AND E
	SIGNAL CINC		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7020";	-- INCREMENT AC
	SIGNAL CSPA		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7010";	-- SKIP NEXT INSTRUCTION IF AC POSITIVE
	SIGNAL CSNA		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7008";	-- SKIP NEXT INSTRUCTION IF AC NEGATIVE
	SIGNAL CSZA		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7004";	-- SKIP NEXT INSTRUCTION IF AC ZERO
	SIGNAL CSZE		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7802";	-- SKIP NEXT INSTRUCTION IF E IS 0
	SIGNAL CHLT		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"7801";	-- HALT COMPUTER
	-- I/O
	SIGNAL CINP		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"F800";	-- INPUT CHARACTER TO AC
	SIGNAL COUT		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"F400";	-- OUTPUT CHARACTER FROM AC
	SIGNAL CSKI		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"F200";	-- SKIP ON INPUT FLAG
	SIGNAL CSKO		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"F100";	-- SKIP ON OUTPUT FLAG
	SIGNAL CION		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"F080";	-- INTERRUPT ON
	SIGNAL CIOF		: STD_LOGIC_VECTOR(15 DOWNTO 0)	:= X"F040";	-- INTERRUPT OFF
	
	-- WIRES AND REGISTERS
	SIGNAL READY_INT	: STD_LOGIC	:= '0';
	SIGNAL FGI_IN	: STD_LOGIC	:= '0';
	SIGNAL FGO_IN	: STD_LOGIC	:= '0';
	SIGNAL PROGRAM	: STD_LOGIC	:= '0';						-- PROGRAM MAIN MEMORY
	SIGNAL COMMAND	: UNSIGNED(7 DOWNTO 0)	:= (OTHERS=>'0');	-- COMMAND TO PROGRAM MAIN MEMROY
	SIGNAL RESULT	: UNSIGNED(7 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL COUNTER	: INTEGER RANGE 0 TO 9;
	
	-- STATE MACHIN
	TYPE PROGRAMER	IS (
		ST0, ST1, ST2, ST3, ST4, ST5, ST6, ST7, ST8, ST9, ST10, ST11, ST12, ST13, ST14,
		ST15, ST16, ST17, ST18, ST19, ST20, ST21, ST22, ST23, ST24, ST25, ST26, ST27, ST28, ST29,
		ST30, ST31, ST32, ST33, ST34, ST35, ST36, ST37, ST38
	);
	SIGNAL PGR		: PROGRAMER;
	
	TYPE GAP	IS (
		G0, G1, G2, G3, G4, G5, G6, G7, G8, G9, G10, G11, G12, G13, G14, G15, G16, G17, G18, G19,
		G20, G21, G22, G23, G24, G25, G26, G27, G28, G29, G30, G31, G32, G33
	);
	SIGNAL GCLK		: GAP		:= G33;
	
	-- CLOCK PERIOD DEFINITIONS
	CONSTANT CLK_PERIOD	:	TIME	:= 100 NS;
	SIGNAL CLOCK	: STD_LOGIC;

BEGIN

	-- CLOCK PROCESS DEFINITIONS
	CLK_PROCESS :PROCESS
	BEGIN
		CLOCK	<=	'0';
		WAIT FOR CLK_PERIOD/2;
		CLOCK	<=	'1';
		WAIT FOR CLK_PERIOD/2;
	END PROCESS;
	
	MICRO_PROCESSOR_INS:
	MANAGEMENT_PROCESSOR	PORT MAP(
		CLOCK, READY_INT, COMMAND, RESULT
	);
	
	PROCESS(CLOCK)
	BEGIN
	
		IF RISING_EDGE(CLOCK)	THEN
			
			READY_INT	<= '0';
		
			CASE PGR	IS
			
				WHEN ST0	=>
					COMMAND	<= X"75";
					READY_INT	<= '1';
					GCLK	<= G0;
					PGR		<= ST36;
				
				WHEN ST1	=>
					READY_INT	<= '1';
					COMMAND	<= X"00";
					GCLK	<= G1;
					PGR		<= ST36;
				
				WHEN ST2	=>
					READY_INT	<= '1';
					COMMAND	<= X"78";
					GCLK	<= G2;
					PGR		<= ST36;
				
				WHEN ST3	=>
					READY_INT	<= '1';
					COMMAND	<= X"20";
					GCLK	<= G3;
					PGR		<= ST36;
				
				WHEN ST4	=>
					READY_INT	<= '1';
					COMMAND	<= X"70";
					GCLK	<= G4;
					PGR		<= ST36;
				
				WHEN ST5	=>
					READY_INT	<= '1';
					COMMAND	<= X"20";
					GCLK	<= G5;
					PGR		<= ST36;
				
				WHEN ST6	=>
					READY_INT	<= '1';
					COMMAND	<= X"70";
					GCLK	<= G6;
					PGR		<= ST36;
				
				WHEN ST7	=>
					READY_INT	<= '1';
					COMMAND	<= X"20";
					GCLK	<= G7;
					PGR		<= ST36;
				
				WHEN ST8	=>
					READY_INT	<= '1';
					COMMAND	<= X"30";
					GCLK	<= G8;
					PGR		<= ST36;
				
				WHEN ST9	=>
					READY_INT	<= '1';
					COMMAND	<= X"20";
					GCLK	<= G9;
					PGR		<= ST36;
				
				WHEN ST10	=>
					READY_INT	<= '1';
					COMMAND	<= X"70";
					GCLK	<= G10;
					PGR		<= ST36;
				
				WHEN ST11	=>
					READY_INT	<= '1';
					COMMAND	<= X"20";
					GCLK	<= G11;
					PGR		<= ST36;
				
				WHEN ST12	=>
					READY_INT	<= '1';
					COMMAND	<= X"10";
					GCLK	<= G12;
					PGR		<= ST36;
				
				WHEN ST13	=>
					READY_INT	<= '1';
					COMMAND	<= X"40";
					GCLK	<= G13;
					PGR		<= ST36;
				
				WHEN ST14	=>
					READY_INT	<= '1';
					COMMAND	<= X"70";
					GCLK	<= G14;
					PGR		<= ST36;
				
				WHEN ST15	=>
					READY_INT	<= '1';
					COMMAND	<= X"16";
					GCLK	<= G15;
					PGR		<= ST36;
				
				WHEN ST16	=>
					READY_INT	<= '1';
					COMMAND	<= X"60";
					GCLK	<= G16;
					PGR		<= ST36;
				
				WHEN ST17	=>
					READY_INT	<= '1';
					COMMAND	<= X"0B";
					GCLK	<= G17;
					PGR		<= ST36;
				
				WHEN ST18	=>
					READY_INT	<= '1';
					COMMAND	<= X"40";
					GCLK	<= G18;
					PGR		<= ST36;
				
				WHEN ST19	=>
					READY_INT	<= '1';
					COMMAND	<= X"21";
					GCLK	<= G19;
					PGR		<= ST36;
				
				WHEN ST20	=>
					READY_INT	<= '1';
					COMMAND	<= X"30";
					GCLK	<= G20;
					PGR		<= ST36;
				
				WHEN ST21	=>
					READY_INT	<= '1';
					COMMAND	<= X"00";
					GCLK	<= G21;
					PGR		<= ST36;
				
				WHEN ST22	=>
					READY_INT	<= '1';
					COMMAND	<= X"78";
					GCLK	<= G22;
					PGR		<= ST36;
					
				WHEN ST23	=>
					READY_INT	<= '1';
					COMMAND	<= X"17";
					GCLK	<= G23;
					PGR		<= ST36;
					
				WHEN ST24	=>
					READY_INT	<= '1';
					COMMAND	<= X"40";
					GCLK	<= G24;
					PGR		<= ST36;
					
				WHEN ST25	=>
					READY_INT	<= '1';
					COMMAND	<= X"FE";
					GCLK	<= G25;
					PGR		<= ST36;
					
				WHEN ST26	=>
					READY_INT	<= '1';
					COMMAND	<= X"FF";
					GCLK	<= G26;
					PGR		<= ST36;
					
				WHEN ST27	=>
					READY_INT	<= '1';
					COMMAND	<= X"21";
					GCLK	<= G27;
					PGR		<= ST36;
					
				WHEN ST28	=>
					READY_INT	<= '1';
					COMMAND	<= X"20";
					GCLK	<= G28;
					PGR		<= ST36;
					
				WHEN ST29	=>
					READY_INT	<= '1';
					COMMAND	<= X"00";
					GCLK	<= G29;
					PGR		<= ST36;
					
				WHEN ST30	=>
					READY_INT	<= '1';
					COMMAND	<= X"F4";
					GCLK	<= G30;
					PGR		<= ST36;
					
				WHEN ST31	=>
					GCLK	<= G31;
					PGR		<= ST36;
					
				WHEN ST32	=>
					READY_INT	<= '1';
					COMMAND	<= X"73";
					GCLK	<= G32;
					PGR		<= ST36;
					
				WHEN ST33	=>
					
				WHEN ST34	=>
				
				WHEN ST35	=>
				
				WHEN ST36	=>
					
				WHEN ST37	=>
				
				WHEN OTHERS	=>
					NULL;
			
			END CASE;
		
			CASE GCLK	IS
				WHEN G0	=>
					PGR		<= ST1;
					GCLK	<= G33;
				WHEN G1	=>
					PGR		<= ST2;
					GCLK	<= G33;
				WHEN G2	=>
					PGR		<= ST3;
					GCLK	<= G33;
				WHEN G3	=>
					PGR		<= ST4;
					GCLK	<= G33;
				WHEN G4	=>
					PGR		<= ST5;
					GCLK	<= G33;
				WHEN G5	=>
					PGR		<= ST6;
					GCLK	<= G33;
				WHEN G6	=>
					PGR		<= ST7;
					GCLK	<= G33;
				WHEN G7	=>
					PGR		<= ST8;
					GCLK	<= G33;
				WHEN G8	=>
					PGR		<= ST9;
					GCLK	<= G33;
				WHEN G9	=>
					PGR		<= ST10;
					GCLK	<= G33;
				WHEN G10	=>
					PGR		<= ST11;
					GCLK	<= G33;
				WHEN G11	=>
					PGR		<= ST12;
					GCLK	<= G33;
				WHEN G12	=>
					PGR		<= ST13;
					GCLK	<= G33;
				WHEN G13	=>
					PGR		<= ST14;
					GCLK	<= G33;
				WHEN G14	=>
					PGR		<= ST15;
					GCLK	<= G33;
				WHEN G15	=>
					PGR		<= ST16;
					GCLK	<= G33;
				WHEN G16	=>
					PGR		<= ST17;
					GCLK	<= G33;
				WHEN G17	=>
					PGR		<= ST18;
					GCLK	<= G33;
				WHEN G18	=>
					PGR		<= ST19;
					GCLK	<= G33;
				WHEN G19	=>
					PGR		<= ST20;
					GCLK	<= G33;
				WHEN G20	=>
					PGR		<= ST21;
					GCLK	<= G33;
				WHEN G21	=>
					PGR		<= ST22;
					GCLK	<= G33;
				WHEN G22	=>
					PGR		<= ST23;
					GCLK	<= G33;
				WHEN G23	=>
					PGR		<= ST24;
					GCLK	<= G33;
				WHEN G24	=>
					PGR		<= ST25;
					GCLK	<= G33;
				WHEN G25	=>
					PGR		<= ST26;
					GCLK	<= G33;
				WHEN G26	=>
					PGR		<= ST27;
					GCLK	<= G33;
				WHEN G27	=>
					PGR		<= ST28;
					GCLK	<= G33;
				WHEN G28	=>
					PGR		<= ST29;
					GCLK	<= G33;
				WHEN G29	=>
					PGR		<= ST30;
					GCLK	<= G33;
				WHEN G30	=>
					PGR		<= ST31;
					GCLK	<= G33;
				WHEN G31	=>
					PGR		<= ST32;
					GCLK	<= G33;
				WHEN G32	=>
					PGR		<= ST33;
					GCLK	<= G33;
					
				WHEN OTHERS	=>
				
			END CASE;
		
		END IF;	-- RISING EDGE
	
	END PROCESS;

END BEHAVIORAL;










