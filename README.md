implementation Morris Mano Architecture

Can be synthesized

using ise design suite 14.7
Tested on the xilinx spartan 6

Basic_Processor.vhd is architecture implementation

Man_Proc.vhd is main to program and manage processor with using UART connection.

After programming Xilinx spartan 6 FPGA with using UART connection bellow programs was tested:
Note: Hex number should be entered.

01- 75Hex        //start
02- 7800         //CCLA
03- 7020         //CINC
04- 7020         //CINC
05- 3020         //CSTA & X"020"
06- 7020         //CINC
07- 1020         //CADD & X"020"
08- 7040         //CCIL
09- 6016        //CISZ & X"016"
10- 400B        //CBUN & X"00B"
11- 3021        //CSTA & X"021"
12- 7800        //CCLA
13- 4017        //CBUN & X"017"
14- FFFE
15- 2021        //CLDA
16- F400        //Cout
17- 7801        //CHLT
18- 73HEX       //stop
from line 2 to line 17, will be store on block memory.
command store on block memory from Addres 10 (A - Hex).
output will be 50-Dec = 32-Hex

01- 75HEX       //start
02- 7800        //CCLA
03- 7020        //CINC
04- 6010        //CISZ
05- 400B        //CBUN
06- F400        //Cout
07- 7801        //HLT
08- FF9C
09- 73HEX       //stop
this program count to 100

01- 75HEX       //start
02- 7800        //CCLA
03- 2019        //CLDA
04- 6019        //CISZ
05- 1019        //CADD
06- 601A        //CISZ
07- 400C        //CBUN
08- 7080        //CCIR
09- 601B        //CISZ
10- 4010        //CBUN
11- F400        //Cout
12- 7080        //CCIR
13- 601C        //CISZ
14- 4014        //CBUN
15- F400        //Cout
16- 7801        //CHLT
17- 000A
18- FF9C
19- FFF8
20- FFF7
21- 72HEX       /stop



