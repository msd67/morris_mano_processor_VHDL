LIBRARY	IEEE;
USE	IEEE.STD_LOGIC_1164.ALL;
USE	IEEE.NUMERIC_STD.ALL;

ENTITY MANAGEMENT_PROCESSOR	IS
	PORT(
		CLK		: IN	STD_LOGIC;
		RX			: IN	STD_LOGIC;
		TX			: OUT	STD_LOGIC
	);
END MANAGEMENT_PROCESSOR;

ARCHITECTURE BEHAVIORAL	OF MANAGEMENT_PROCESSOR	IS

	component MY_CLOCK
	port
	(-- Clock in ports
		CLK_IN1           : in     std_logic;
		-- Clock out ports
		CLK_OUT1          : out    std_logic
	);
	end component;

	component ICON
	PORT (
		CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
	end component;
	
component ILA
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    DATA : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    TRIG0 : IN STD_LOGIC_VECTOR(0 TO 0));

end component;

	-- MONITORING
	SIGNAL CONTROL0			: STD_LOGIC_VECTOR(35 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL TRIG0			: STD_LOGIC_VECTOR(0 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL CLOCK			: STD_LOGIC;
	SIGNAL AR_CHECK			: UNSIGNED(15 DOWNTO 0)			:= (OTHERS=>'0');
	
	-- BASIC PROCESSOR
	SIGNAL RESET			: STD_LOGIC;
	SIGNAL FGI_I			: STD_LOGIC;
	SIGNAL FGO_I			: STD_LOGIC;
	SIGNAL PROGRAM_MODE		: STD_LOGIC;
	SIGNAL COMMAND			: UNSIGNED(15 DOWNTO 0);
	SIGNAL SEND				: UNSIGNED(07 DOWNTO 0);
	SIGNAL FGI_O			: STD_LOGIC;
	SIGNAL FGO_O			: STD_LOGIC;
	SIGNAL RECV				: UNSIGNED(07 DOWNTO 0);
	
	-- RX SERIAL
	SIGNAL DATA_IN			: UNSIGNED(07 DOWNTO 0);
	SIGNAL READY			: STD_LOGIC;
	SIGNAL RX_INT			: STD_LOGIC;
	
	-- TX SERIAL
	SIGNAL DATA_OUT			: UNSIGNED(07 DOWNTO 0);
	SIGNAL SEND_INT			: STD_LOGIC;
	SIGNAL BUSY_INT			: STD_LOGIC;
	SIGNAL TX_INT			: STD_LOGIC;
	
	-- INTERNAL PRACTICAL REGISTER
	SIGNAL INP_COMP			: UNSIGNED(01 DOWNTO 0);
	SIGNAL DATA_COMP		: UNSIGNED(15 DOWNTO 0);
	SIGNAL PROGRAMMING		: STD_LOGIC;
	SIGNAL DATA_HOLD		: UNSIGNED(07 DOWNTO 0);
	SIGNAL IN_FLAG			: STD_LOGIC;
	SIGNAL BUFF_FLG			: STD_LOGIC		:= '0';
	SIGNAL FLG2				: STD_LOGIC		:= '0';
	SIGNAL FLG3				: STD_LOGIC		:= '0';
	SIGNAL FLG4				: STD_LOGIC		:= '0';
	SIGNAL FLG5				: STD_LOGIC		:= '0';
	SIGNAL WAITING_BUFF		: UNSIGNED(07 DOWNTO 0)	:= (OTHERS=>'0');

BEGIN

	TRIG0(0)	<= FLG5;

	PROCESS(CLOCK)
	BEGIN
	
		IF RISING_EDGE(CLOCK)	THEN
		
			RX_INT		<= RX;
			TX			<= TX_INT;
			SEND_INT	<= '0';
			RESET		<= '0';
			FGO_I		<= '0';
			
			IF READY='1'	THEN
				INP_COMP	<= INP_COMP + 1;
				IF INP_COMP="00"	THEN
					DATA_COMP(07 DOWNTO 0)	<= DATA_IN;
				ELSIF INP_COMP="01"	THEN
					DATA_COMP(15 DOWNTO 8)	<= DATA_IN;
				END IF;
				
				-- START ENTERING COMMAND TO PROGRAMMING
				IF DATA_IN=X"75"	THEN
					INP_COMP	<= "00";
					DATA_COMP	<= (OTHERS=>'0');
					PROGRAMMING	<= '1';
					RESET		<= '1';
				END IF;
				-- END OF ENTERING COMMAND
				IF DATA_IN=X"73"	THEN
					PROGRAMMING	<= '0';
					DATA_COMP	<= (OTHERS=>'0');
					INP_COMP	<= "00";
					RESET		<= '1';
				END IF;
				-- INPUT VALUE TO PROCESSOR
				IF DATA_IN=X"79"	THEN
					DATA_COMP	<= (OTHERS=>'0');
					INP_COMP	<= "00";
					IF FGI_O='0'	THEN
						FGI_I	<= '1';
						SEND	<= DATA_COMP(07 DOWNTO 0);
					ELSE
						IN_FLAG	<= '1';
						DATA_HOLD	<= DATA_COMP(07 DOWNTO 0);
					END IF;
				END IF;
				-- RESEND DATA OUT
				IF DATA_IN=X"77"	THEN
					SEND_INT	<= '1';
					DATA_COMP	<= (OTHERS=>'0');
					INP_COMP	<= "00";
				END IF;
			END IF;
			
			-- WAIT FOR CLEAR INPUT REGISTER
			IF FGI_O='0'	AND IN_FLAG='1'	THEN
				IN_FLAG		<= '0';
				FGI_I		<= '1';
				SEND		<= DATA_HOLD;
			END IF;
			
			-- OUTPUT RESULT FROM PROCESSOR
			IF FGO_O='0'	THEN
				IF BUSY_INT='0'	THEN
					FGO_I		<= '1';
					DATA_OUT	<= RECV;
					SEND_INT	<= '1';
				ELSE
					FGO_I					<= '1';
					WAITING_BUFF			<= RECV;
					BUFF_FLG				<= '1';
				END IF;
			END IF;
			IF BUFF_FLG='1'	AND BUSY_INT='0'	THEN
				DATA_OUT	<= WAITING_BUFF;
				SEND_INT	<= '1';
				BUFF_FLG	<= '0';	
				FGO_I		<= '1';
				FLG2		<= '1';
			END IF;
			IF FLG2='1'	THEN
				--SEND_INT	<= '1';
				FLG2		<= '0';
				FLG3		<= '1';
			END IF;
			IF FLG3='1'	THEN
				--SEND_INT	<= '1';
				FLG3		<= '0';
				FLG4		<= '1';
			END IF;
			IF FLG4='1'	THEN
				SEND_INT	<= '1';
				FLG4		<= '0';
				FLG5		<= '1';
			END IF;
			
			PROGRAM_MODE	<= '0';
			IF INP_COMP="10"	AND PROGRAMMING='1'	THEN
				PROGRAM_MODE	<= '1';
				COMMAND			<= DATA_COMP(07 DOWNTO 0) & DATA_COMP(15 DOWNTO 8);
				INP_COMP		<= "00";
			END IF;
		
		END IF;	-- RISING EDGE
		
	END PROCESS;

	BASIC_PROC:	ENTITY	WORK.BASIC_PROCESSOR(BEHAVIORAL)
	PORT MAP(
		RESET		=> RESET,
		CLOCK		=> CLOCK,
		FGI_IN		=> FGI_I,
		FGO_IN		=> FGO_I,
		PROGRAM		=> PROGRAM_MODE,
		COMMAND		=> COMMAND,
		SEND_MID	=> SEND,
		FGI_O		=> FGI_O,
		FGO_O		=> FGO_O,
		AR_CHECK	=> AR_CHECK,
		RECV_MID	=> RECV
	);

	RX_Serial:	ENTITY	work.RS_232_Rx(Behavioral)
	PORT MAP(
		Clock	=> CLOCK,
		Data	=> DATA_IN,
		Valid	=> READY,
		Serial	=> RX_INT
	);
	
	TX_Serial:	ENTITY	work.RS_232_Tx(Behavioral)
	PORT MAP(
		Clock	=> CLOCK,
		Data	=> DATA_OUT,
		Send	=> SEND_INT,
		Busy	=> BUSY_INT,
		Serial	=> TX_INT
	);
	
	MY_ICON : ICON
		port map (
			CONTROL0 => CONTROL0);

MY_ILA : ILA
  port map (
    CONTROL => CONTROL0,
    CLK => CLOCK,
    DATA => X"0" & STD_LOGIC_VECTOR(FGO_O & BUFF_FLG & BUSY_INT & SEND_INT & DATA_OUT),
    TRIG0 => TRIG0);
		
	MY_CLOCK_INS : MY_CLOCK
		port map
		(	-- Clock in ports
			CLK_IN1 => CLK,
			-- Clock out ports
			CLK_OUT1 => CLOCK);
		
END BEHAVIORAL;