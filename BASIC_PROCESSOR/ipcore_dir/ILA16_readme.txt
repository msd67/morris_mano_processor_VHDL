The following files were generated for 'ILA16' in directory
C:\Users\masoud\Documents\FPGA_T\Personal Training\PROCESSOR_XILINX\BASIC_PROCESSOR\ipcore_dir\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * ILA16.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * ILA16.cdc
   * ILA16.constraints/ILA16.ucf
   * ILA16.constraints/ILA16.xdc
   * ILA16.ncf
   * ILA16.ngc
   * ILA16.ucf
   * ILA16.vhd
   * ILA16.vho
   * ILA16.xdc
   * ILA16_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * ILA16.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * ILA16.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * ILA16.gise
   * ILA16.xise
   * _xmsgs/pn_parser.xmsgs

Deliver Readme:
   Readme file for the IP.

   * ILA16_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * ILA16_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

