LIBRARY	IEEE;
USE	IEEE.STD_LOGIC_1164.ALL;
USE	IEEE.NUMERIC_STD.ALL;

ENTITY BASIC_PROCESSOR	IS
	PORT (
		RESET		: IN	STD_LOGIC;
		CLOCK		: IN	STD_LOGIC;
		FGI_IN		: IN	STD_LOGIC;
		FGO_IN		: IN	STD_LOGIC;
		PROGRAM		: IN	STD_LOGIC;				-- PROGRAM MAIN MEMORY
		COMMAND		: IN	UNSIGNED(15 DOWNTO 0);	-- COMMAND TO PROGRAM MAIN MEMROY
		SEND_MID	: IN	UNSIGNED(7 DOWNTO 0);
		FGI_O		: OUT	STD_LOGIC;
		FGO_O		: OUT	STD_LOGIC;
		AR_CHECK	: OUT	UNSIGNED(15 DOWNTO 0);
		RECV_MID	: OUT	UNSIGNED(7 DOWNTO 0)
	);
END BASIC_PROCESSOR;

ARCHITECTURE BEHAVIORAL	OF BASIC_PROCESSOR	IS

	COMPONENT CONTROL_UNIT	IS
	PORT(
			IR		: IN	UNSIGNED(15 DOWNTO 0);	-- FROM INSTRUCTION REGISTER
			AC		: IN	UNSIGNED(15 DOWNTO 0);	-- FROM ACCUMULATOR
			DR		: IN	UNSIGNED(15 DOWNTO 0);	-- FROM DATA REGISTER
			E		: IN	STD_LOGIC;						-- FROM ACCUMULATOR
			IEN		: IN	STD_LOGIC;
			FGI		: IN	STD_LOGIC;
			FGO		: IN	STD_LOGIC;
			CLOCK	: IN	STD_LOGIC;
			RESET	: IN	STD_LOGIC;
			PRG		: IN	STD_LOGIC;
			AC_CLR	: OUT	STD_LOGIC;
			AC_INR	: OUT	STD_LOGIC;
			AC_AND	: OUT	STD_LOGIC;
			AC_ADD	: OUT	STD_LOGIC;
			AC_DR	: OUT	STD_LOGIC;
			AC_INPR	: OUT	STD_LOGIC;	-- INPUT REGISTER TO AC & FGI=0
			AC_COM	: OUT	STD_LOGIC;
			AC_SHR	: OUT	STD_LOGIC;
			AC_SHL	: OUT	STD_LOGIC;
			MEM_WR	: OUT	STD_LOGIC;
			MEM_RD	: OUT	STD_LOGIC;
			AR_LD	: OUT	STD_LOGIC;
			AR_INR	: OUT	STD_LOGIC;
			AR_CLR	: OUT	STD_LOGIC;
			PC_LD	: OUT	STD_LOGIC;
			PC_INR	: OUT	STD_LOGIC;
			PC_CLR	: OUT	STD_LOGIC;
			DR_LD	: OUT	STD_LOGIC;
			DR_INR	: OUT	STD_LOGIC;
			IR_LD	: OUT	STD_LOGIC;
			TR_LD	: OUT	STD_LOGIC;
			OUTR_LD	: OUT	STD_LOGIC;	-- LOAD OUTR & FGO=0
			IEN_ON	: OUT	STD_LOGIC;
			IEN_OFF	: OUT	STD_LOGIC;
			HALT	: OUT	STD_LOGIC;
			NE		: OUT	STD_LOGIC;
			ZE		: OUT	STD_LOGIC;
			S_BUS	: OUT	UNSIGNED(2 DOWNTO 0)
		);
	END COMPONENT;

	-- PROCESSOR REGISTERS
	SIGNAL AC		: UNSIGNED(15 DOWNTO 0);	-- ACCUMULATOR
	SIGNAL E		: STD_LOGIC;				-- ACCUMULATOR CARRY OUT
	SIGNAL AR		: UNSIGNED(11 DOWNTO 0);	-- ADDRESS REGISTER
	SIGNAL PC		: UNSIGNED(11 DOWNTO 0);	-- PROGRAM COUNTER
	SIGNAL DR		: UNSIGNED(15 DOWNTO 0);	-- DATA REGISTER
	SIGNAL IR		: UNSIGNED(15 DOWNTO 0);	-- INSTRUCTION REGISTER
	SIGNAL TR		: UNSIGNED(15 DOWNTO 0);	-- TEMPORARY REGISTER
	SIGNAL INPR		: UNSIGNED(7 DOWNTO 0);		-- INPUT REGISTER
	SIGNAL OUTR		: UNSIGNED(7 DOWNTO 0);		-- OUTPUT REGISTER
	SIGNAL IEN		: STD_LOGIC;				-- INTRRUPT ENABLE
	SIGNAL FGI		: STD_LOGIC;				-- INPUT FLAG
	SIGNAL FGO		: STD_LOGIC;				-- OUTPUT FLAG
	SIGNAL S		: STD_LOGIC;				-- HALT
	
	SIGNAL DATA_BUS	: UNSIGNED(15 DOWNTO 0);
	SIGNAL S_BUS	: UNSIGNED(2 DOWNTO 0);		-- BUS MANAGEMENT
	SIGNAL MEM_OTD	: UNSIGNED(15 DOWNTO 0);	-- MEMORY DATA OUTPUT
	
	SIGNAL AC1		: UNSIGNED(16 DOWNTO 0);	-- ACCUMULATOR WITH E (CARRY)
	
	-- PRGRAM MICRO CONTROLLER
	SIGNAL PRG_CNT	: UNSIGNED(11 DOWNTO 0);
	SIGNAL PRG_WR	: STD_LOGIC;
	SIGNAL WR_INT	: STD_LOGIC;
	SIGNAL PRE_PROG	: STD_LOGIC;
	
	-- WIRES FROM CONTROL UNIT
	SIGNAL AC_CLR	: STD_LOGIC;
	SIGNAL AC_INR	: STD_LOGIC;
	SIGNAL AC_AND	: STD_LOGIC;
	SIGNAL AC_ADD	: STD_LOGIC;
	SIGNAL AC_DR	: STD_LOGIC;
	SIGNAL AC_INPR	: STD_LOGIC;
	SIGNAL AC_COM	: STD_LOGIC;
	SIGNAL AC_SHR	: STD_LOGIC;
	SIGNAL AC_SHL	: STD_LOGIC;
	SIGNAL MEM_WR	: STD_LOGIC;	-- WE_INT, WRITE ENABLE
	SIGNAL AR_LD	: STD_LOGIC;
	SIGNAL AR_INR	: STD_LOGIC;
	SIGNAL AR_CLR	: STD_LOGIC;
	SIGNAL PC_LD	: STD_LOGIC;
	SIGNAL PC_INR	: STD_LOGIC;
	SIGNAL PC_CLR	: STD_LOGIC;
	SIGNAL DR_LD	: STD_LOGIC;
	SIGNAL DR_INR	: STD_LOGIC;
	SIGNAL IR_LD	: STD_LOGIC;
	SIGNAL TR_LD	: STD_LOGIC;
	SIGNAL OUTR_LD	: STD_LOGIC;
	SIGNAL IEN_ON	: STD_LOGIC;
	SIGNAL IEN_OFF	: STD_LOGIC;
	SIGNAL HALT		: STD_LOGIC;
	SIGNAL NE		: STD_LOGIC;
	SIGNAL ZE		: STD_LOGIC;
	SIGNAL PRG_CNTR	: STD_LOGIC;	-- STOP CONTROLL UNIT

BEGIN

	CONTROL_UNIT_INS:
	CONTROL_UNIT	PORT MAP(
		IR, AC, DR, E, IEN, FGI, FGO, CLOCK, RESET, PRG_CNTR, AC_CLR, AC_INR,
		AC_AND, AC_ADD, AC_DR, AC_INPR, AC_COM, AC_SHR, AC_SHL,
		MEM_WR, OPEN, AR_LD, AR_INR, AR_CLR, PC_LD, PC_INR,		-- MEMORY READ IS OPEN
		PC_CLR, DR_LD, DR_INR, IR_LD, TR_LD,
		OUTR_LD, IEN_ON, IEN_OFF, HALT, NE, ZE, S_BUS
	);
	
	Inst_Block_RAM: ENTITY work.BRAM 
			GENERIC MAP
					(
							RAM_Width				=>	16,
							RAM_AddWidth			=>	12
					)
			PORT MAP( 
							Clock 					=> 	Clock,
							WE 						=> 	WR_INT,
							Write_Address 			=> 	AR,
							Read_Address 			=> 	AR,
							Data_In 				=> 	DATA_BUS,
							Data_Out 				=> 	MEM_OTD
						);
	
	WR_INT		<= MEM_WR	OR PRG_WR;
	
	RECV_MID	<= OUTR;
	
	AC			<= AC1(15 DOWNTO 0);
	E			<= AC1(16);
	
	FGI_O		<= FGI;
	FGO_O		<= FGO;
	
	AR_CHECK	<= AC(3 DOWNTO 0) & "00" & OUTR_LD & FGO & OUTR;
	
	PROCESS(CLOCK)
	BEGIN
			
		IF RISING_EDGE(CLOCK)	THEN
			
			-- ACCUMULATOR REGISTER
			IF AC_AND='1'	THEN
				AC1(15 DOWNTO 0)	<= AC1(15 DOWNTO 0)	AND DR;
			END IF;
			IF AC_ADD='1'	THEN
				AC1					<= AC1	+ DR;
			END IF;
			IF AC_DR='1'	THEN
				AC1(15 DOWNTO 0)	<= DR;
			END IF;
			IF AC_INPR='1'	THEN
				AC1(07 DOWNTO 0)	<= INPR;
			END IF;
			IF AC_COM='1'	THEN
				AC1(15 DOWNTO 0)	<= NOT AC1(15 DOWNTO 0);
			END IF;
			IF AC_SHR='1'	THEN
				AC1					<= AC1(0)	& AC1(16 DOWNTO 1);
			END IF;
			IF AC_SHL='1'	THEN
				AC1					<= AC1(15 DOWNTO 0)	& AC1(16);
			END IF;
			IF AC_CLR='1'	THEN
				AC1					<= (OTHERS=>'0');
			END IF;
			IF AC_INR='1'	THEN
				AC1					<= AC1 + 1;
			END IF;
			IF NE='1'		THEN
				AC1(16)				<= NOT AC1(16);
			END IF;
			IF ZE='1'		THEN
				AC1(16)				<= '0';
			END IF;
			IF RESET='1'	THEN
				AC1					<= (OTHERS=>'0');
			END IF;
			
			
			-- ADDRESS REGISTER
			IF AR_LD='1'	THEN
				AR		<= DATA_BUS(11 DOWNTO 0);
			END IF;
			IF AR_INR='1'	THEN
				AR		<= AR + 1;
			END IF;
			IF AR_CLR='1'	THEN
				AR		<= X"000";
			END IF;
			IF RESET='1'	THEN
				AR		<= X"000";
			END IF;
			
			-- PROGRAM COUNTER
			IF PC_LD='1'	THEN
				PC		<= DATA_BUS(11 DOWNTO 0);
			END IF;
			IF PC_INR='1'	THEN
				PC		<= PC + 1;
			END IF;
			IF PC_CLR='1'	THEN
				PC		<= X"000";
			END IF;
			IF S='0'		THEN
				PC		<= X"000";
			END IF;
			IF RESET='1'	THEN
				PC		<= X"00A";
			END IF;
			
			-- DATA REGISTER
			IF DR_LD='1'	THEN
				DR		<= DATA_BUS;
			END IF;
			IF DR_INR='1'	THEN
				DR		<= DR + 1;
			END IF;
			IF RESET='1'	THEN
				DR		<= X"0000";
			END IF;
			
			-- INSTRUCTION REGISTER
			IF IR_LD='1'	THEN
				IR		<= DATA_BUS;
			END IF;
			IF RESET='1'	THEN
				IR		<= X"0000";
			END IF;
			
			-- TEMPORARY REGISTER
			IF TR_LD='1'	THEN
				TR		<= DATA_BUS;
			END IF;
			IF RESET='1'	THEN
				TR		<= X"0000";
			END IF;
			
			-- INPUT FLAG
			IF AC_INPR='1'	THEN
				FGI		<= '0';
			END IF;
			IF FGI_IN='1'	THEN
				FGI		<= '1';
			END IF;
			IF RESET='1'	THEN
				FGI		<= '0';
			END IF;
			
			-- INPUT REGISTER
			IF FGI_IN='1'	THEN
				INPR	<= SEND_MID;
			END IF;
			IF RESET='1'	THEN
				INPR	<= (OTHERS=>'0');
			END IF;
			
			-- OUTPUT FLAG
			IF OUTR_LD='1'	THEN
				FGO		<= '0';
			END IF;
			IF FGO_IN='1'	THEN
				FGO		<= '1';
			END IF;
			IF RESET='1'	THEN
				FGO		<= '1';
			END IF;
			
			-- OUTPUT REGISTER
			IF OUTR_LD='1'	THEN
				OUTR	<= DATA_BUS(7 DOWNTO 0);
			END IF;
			IF RESET='1'	THEN
				OUTR	<= (OTHERS=>'0');
			END IF;
			
			-- INTRRUPT ENABLE
			IF IEN_OFF='1'	THEN
				IEN		<= '0';
			END IF;
			IF IEN_ON='1'	THEN
				IEN		<= '1';
			END IF;
			IF RESET='1'	THEN
				IEN		<= '1';
			END IF;
			
			-- HALT
			IF HALT='1'		THEN
				S		<= '0';
			END IF;
			IF RESET='1'	THEN
				S		<= '1';
			END IF;
			
			-- PROGRAM MICRO CONTROLLER
			PRE_PROG	<= PROGRAM;
			PRG_WR		<= '0';
			IF PROGRAM='1'	AND PRE_PROG='0'	THEN
				PRG_WR		<= '1';
				AR			<= PRG_CNT;
				PC			<= X"000";
				--DATA_BUS	<= COMMAND;
				PRG_CNT		<= PRG_CNT + 1;
				PRG_CNTR	<= '1';
			END IF;
			IF RESET='1'	THEN
				PRG_CNT		<= X"00A";
				PRG_CNTR	<= '0';
			END IF;
			
		END IF;	-- RISING EDGE

	END PROCESS;
	
	DATA_BUS	<=	X"0000"		WHEN S='0'			ELSE
					X"0000"		WHEN RESET='1'		ELSE
					COMMAND		WHEN PRG_CNTR='1'	ELSE
					X"0" & AR	WHEN S_BUS="001"	ELSE
					X"0" & PC	WHEN S_BUS="010"	ELSE
					DR			WHEN S_BUS="011"	ELSE
					AC			WHEN S_BUS="100"	ELSE
					IR			WHEN S_BUS="101"	ELSE
					TR			WHEN S_BUS="110"	ELSE
					MEM_OTD		WHEN S_BUS="111"	ELSE
					X"0000";

END BEHAVIORAL;